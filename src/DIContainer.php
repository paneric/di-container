<?php

declare(strict_types=1);

namespace Paneric\DIContainer;

use Psr\Container\ContainerInterface;

class DIContainer implements ContainerInterface
{
    private $container = [];

    private $mapper = [];

    public function get($id)
    {
        if ($this->has($id)) {
            return $this->getInstance($id);
        }

        return null;
    }

    public function set(string $id, $item): void
    {
        if (isset($this->mapper[$id])) {
            return;
        }

        if (is_object($item)) {
            $this->container[$id] = $item;

            $this->mapper[$id] = function($i) {
                return $this->get($i);
            };

            return;
        }

        $this->mapper[$id] = $item;
    }

    public function has($id): bool
    {
        return isset($this->mapper[$id]);
    }

    public function merge(array $dependencies): void
    {
        if (empty($dependencies)) {
            return;
        }

        foreach ($dependencies as $id => $closure) {
            if (!isset($this->container[$id])) {
                $this->mapper[$id] = $closure;
            }
        }
    }

    private function getInstance(string $id)
    {
        if (isset($this->container[$id])) {
            return $this->container[$id];
        }

        $closure = $this->mapper[$id];

        if (is_array($closure) || is_string($closure)) {
            $this->container[$id] = $this->mapper[$id];

            return $this->container[$id];
        }

        $this->container[$id] = $this->mapper[$id]($this);

        return $this->container[$id];
    }
}
